import logging
from random import choice
from requests import Session
from django.conf import settings
from .spotify import SpotifyClient

# Configuración de logging
logging.basicConfig(level=logging.INFO)

GRAPH_API_URL = 'https://graph.facebook.com'
GRAPH_API_VERSION = 'v18.0'


class ApiGraph:
    def __init__(self, access_token=None):
        """Inicializar ApiGraph con un access_token opcional."""
        self.access_token = access_token if access_token else settings.META_ACCESS_TOKEN
        self.session = Session()

    def _get_full_url(self, endpoint):
        """Construir la URL completa para la API de Facebook Graph."""
        return f'{GRAPH_API_URL}/{GRAPH_API_VERSION}/{endpoint}'

    def setup(self):
        """Configurar el perfil del messenger en la API de Facebook Graph."""
        url = self._get_full_url('me/messenger_profile')
        response = self.session.self.session.post(
            url,
            params={'access_token': self.access_token},
            json={
                'get_started': {'payload': 'GET_STARTED_PAYLOAD'},
                'greeting': [{'locale': 'default', 'text': 'Hola {{user_full_name}}'}]
            }
        )
        if response.status_code == 200:
            return response.json()

        logging.error('Error en la configuración del perfil del messenger')
        return response.text

    # https://developers.facebook.com/docs/messenger-platform/send-messages/sender-actions/
    def send_action(self, recipient_id, action):
        response = self.session.post(
            f'{self.url}/{self.version}/me/messages',
            params={
                'access_token': self.access_token
            },
            json={
                'recipient': {
                    'id': recipient_id
                },
                'sender_action': action
            }
        )
        return response.json()

    # https://developers.facebook.com/docs/messenger-platform/reference/send-api/
    def send_message(self, recipient_id, body):
        response = self.session.post(
            f'{self.url}/{self.version}/me/messages',
            params={
                'access_token': self.access_token
            },
            json={
                'recipient': {
                    'id': recipient_id
                },
                'messaging_type': 'RESPONSE',
                'message': body
            }
        )
        return response.json()

    def quick_reply_message(self, recipient_id, message, options):
        return self.send_message(
            recipient_id,
            {
                'text': message,
                "quick_replies": options
            }
        )

    def __options(self):
        return [
            {
                'content_type': 'text',
                'title': 'Buscar Musica',
                'payload': 'SEARCH_MUSIC',
                'image_url': 'https://cdn-icons-png.flaticon.com/512/4430/4430494.png'
            },
            {
                'content_type': 'text',
                'title': 'Conversar',
                'payload': 'TALK_MESSAGE',
                'image_url': 'https://cdn-icons-png.flaticon.com/512/1380/1380370.png'
            }
        ]

    def welcome_message(self, recipient_id):
        return self.quick_reply_message(
            recipient_id,
            'Hola, ¿Que deseas hacer?',
            self.__options()
        )

    def retry_options(self, recipient_id):
        return self.quick_reply_message(
            recipient_id,
            'Ahora, que deseas hacer?',
            self.__options()
        )

    def search_music_message(self, recipient_id):
        return self.send_message(
            recipient_id,
            {
                'text': 'Escriba el nombre de la canción o del artista: '
            }
        )

    def talk_message(self, recipient_id):
        messages = [
            'Estas bien ?',
            'Ella no te ama',
            'Funciona !!!'
        ]

        return self.send_message(
            recipient_id,
            {
                'text': choice(messages)
            }
        )

    def spotify_message(self, recipient_id, query):
        spotify = SpotifyClient()
        results = spotify.search_track(query)
        tracks = results['tracks']['items']
        elements = self.__template_elements(tracks)

        return self.send_message(
            recipient_id,
            {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": elements
                    }
                }
            }
        )

    def __template_elements(self, tracks):
        elements = []
        for track in tracks:
            artist = track['artists'][0]['name']
            track_name = track['name']
            album_image = track['album']['images'][0]['url']
            album_name = track['album']['name']
            track_url = track['external_urls']['spotify']

            elements.append({
                'title': f'{artist} - {track_name}',
                'image_url': album_image,
                'subtitle': album_name,
                'default_action': {
                    'type': 'web_url',
                    'url': track_url,
                    'webview_height_ratio': 'tall'
                },
                'buttons': [
                    {
                        'type': 'web_url',
                        'url': track_url,
                        'title': 'Play Spotify'
                    }
                ]
            })
        return elements
