from rest_framework import generics, status
from rest_framework.response import Response
from django.conf import settings
from application.utils.apigraph import ApiGraph
from time import sleep


apigraph = ApiGraph()


class SetupView(generics.GenericAPIView):
    serializer_class = None
    http_method_names = ['get']

    def get(self, _):
        apigraph.setup()
        return Response(status=status.HTTP_200_OK)


class WebhookView(generics.GenericAPIView):
    serializer_class = None
    http_method_names = ['get', 'post']

    def get(self, request):
        query_params = request.query_params
        hub_mode = query_params.get('hub.mode')
        hub_challenge = query_params.get('hub.challenge')
        hub_verify_token = query_params.get('hub.verify_token')

        if hub_mode == 'subscribe' and hub_verify_token == settings.META_VERIFY_TOKEN:
            return Response(data=int(hub_challenge), status=status.HTTP_200_OK)

        return Response(status=status.HTTP_403_FORBIDDEN)

    def post(self, request):
        data = request.data
        for entry in data['entry']:
            messaging = entry['messaging']
            for message in messaging:
                sender_id = message['sender']['id']
                msg = message.get('message')
                postback = message.get('postback')

                apigraph.send_action(sender_id, 'mark_seen')
                sleep(1)
                apigraph.send_action(sender_id, 'typing_on')
                sleep(1)
                apigraph.send_action(sender_id, 'typing_off')

                if postback:
                    self.postback_event(sender_id, postback)
                else:
                    self.message_event(sender_id, msg)

        return Response(status=status.HTTP_200_OK)

    def postback_event(self, sender_id, postback):
        payload = postback.get('payload')

        if payload == 'GET_STARTED_PAYLOAD':
            return apigraph.welcome_message(sender_id)

    def message_event(self, sender_id, message):
        quick_reply = message.get('quick_reply')

        if quick_reply:
            return self.quickreply_event(sender_id, quick_reply)

        text = message.get('text')
        apigraph.spotify_message(sender_id, text)
        return apigraph.retry_options(sender_id)

    def quickreply_event(self, sender_id, quick_reply):
        payload = quick_reply.get('payload')

        if payload == 'SEARCH_MUSIC':
            return apigraph.search_music_message(sender_id)

        if payload == 'TALK_MESSAGE':
            apigraph.talk_message(sender_id)

        return apigraph.retry_options(sender_id)
