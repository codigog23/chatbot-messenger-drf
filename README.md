# BOT Messenger

## Recursos

- ApiGraph Meta
- Spotipy

## .env

```py
META_VERIFY_TOKEN=''
META_ACCESS_TOKEN=''

SPOTIFY_CLIENT_ID=''
SPOTIFY_CLIENT_SECRET=''
```
